# Notes and Notebooks for [Cornell Birdcall Identification](https://www.kaggle.com/c/birdsong-recognition/overview)

From the competition page:
>Your challenge in this competition is to identify which birds are calling in long recordings, given training data generated in meaningfully different contexts. This is the exact problem facing scientists trying to automate the remote monitoring of bird populations.
